/*
it should have a function to display todos
it should have a function to add todos
it should have a fuction to change todos
it should have a function delete todos
*/

var todos = ["item 1","item 2","item 3"]

function displayTodos() {
    console.log("My Todos: ", todos);
}

function addTodo(todo) {
    todos.push(todo);
    displayTodos();
}

function changeTodo(position, newValue){
    todos[position] = newValue;
    displayTodos();
}

function deleteTodo(position){
    todos.splice(position, 1);
    displayTodos();
}

